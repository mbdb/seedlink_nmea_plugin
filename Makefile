DIRS = libmseed src
PLUG_BINS = $(SEISCOMP_ROOT)/share/plugins/seedlink
TEMPLATES = $(SEISCOMP_ROOT)/share/templates/seedlink/nmea
DESCRIPTIONS = $(SEISCOMP_ROOT)/etc/descriptions

all: 
	@for d in $(DIRS) ; do \
	    echo "Running $(MAKE) $@ in $$d" ; \
	    if [ -f $$d/Makefile -o -f $$d/makefile ] ; \
	        then ( cd $$d && $(MAKE) $@ ) ; \
	    elif [ -d $$d ] ; \
	        then ( echo "ERROR: no Makefile/makefile in $$d for $(CC)" ) ; \
	    fi ; \
	done

install:
	test -d $(TEMPLATES) || mkdir -p $(TEMPLATES)
	cp setup.py $(TEMPLATES)
	cp seedlink_plugin.tpl $(TEMPLATES)
	cp plugins.ini.tpl $(TEMPLATES)
	cp seedlink_nmea.xml $(DESCRIPTIONS)
	cp src/nmea_plugin $(PLUG_BINS)

clean:
	@for d in $(DIRS) ; do \
	    echo "Running $(MAKE) $@ in $$d" ; \
	    if [ -f $$d/Makefile -o -f $$d/makefile ] ; \
	        then ( cd $$d && $(MAKE) $@ ) ; \
	    elif [ -d $$d ] ; \
	        then ( echo "ERROR: no Makefile/makefile in $$d for $(CC)" ) ; \
	    fi ; \
	done
	rm -rf $(TEMPLATES)
	rm -rf $(DESCRIPTIONS)/seedlink_nmea.xml

