#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

#include "libmseed.h"
#include "plugin.h"

#define NMEASIZE 82
#define BUFSAMPLES 112
#define MAX_STRING_LENGTH 128

#define VERSION "0.1"

int32_t bind_data(char *sentence, char *map, int index);
void map2kfc(char *map, char *key, int *field, double *sr, char* chanid, int index);
int options(int argc, char *argv[], char *address, char *port, char *id, char *map);
int record_handler(char *record, int length, void* handlerdata);
int sendrecord(char *staid, char*chanid, double sr, int32_t *buffer, struct timespec *timestamp);
void sig_handler(int signum);
FILE *sockconnect(char *address, char *port);

volatile bool go = true;

int main(int argc, char *argv[])
{
    //vars
    int i, j, k, n, data, field;
    double sr;
    //options
    char *address = malloc(MAX_STRING_LENGTH);
    char *port = malloc(MAX_STRING_LENGTH);
    char *id = malloc(MAX_STRING_LENGTH);
    char *map = malloc(MAX_STRING_LENGTH);
    char *key = malloc(6);
    char *chanid = malloc(6);
    char *temp = malloc(MAX_STRING_LENGTH);
    //socket file
    FILE *fsock;
    //acquisition words, buffer & timestamp
    char *sentence = malloc(NMEASIZE);
    char *word;
    int32_t *buffer, *pack = malloc(BUFSAMPLES*sizeof(int32_t));
    struct timespec *timestamp = malloc(sizeof(struct timespec));

    //parse options
    if (options(argc, argv, address, port, id, map) < 0)
    {
        perror("ERROR parsing options or keyfile");
        goto error;
    }

    //get number of channels to acquire
    n = 0;
    strcpy(temp, map);
    word = strtok(temp, ";");
    if (word == NULL)
    {
        perror("ERROR in mapping definition");
        goto error;
    }
    else
    {
        while (word != NULL)
        {
            n++;
            word = strtok(NULL, ";");
        }
    }
    //allocate suitable buffer
    buffer = malloc(n*BUFSAMPLES*sizeof(int32_t));

    //socket connection
    fsock = sockconnect(address, port);
    if (fsock == NULL)
    {
        perror("ERROR connecting socket");
        goto error;
    }

    signal(SIGINT, &sig_handler);
    signal(SIGQUIT, &sig_handler);
    //main loop
    while (go)
    {
        i=0;
        k=0;
        while ((i<BUFSAMPLES) && (go))
        {
            //read NMEA sentence
            if (fgets(sentence, NMEASIZE, fsock) == NULL) break;
            //timestamp first sample
            if (i==0)
            {
                if (clock_gettime(CLOCK_REALTIME, timestamp) != 0)
                {
                    perror("ERROR collecting time");
                    break;
                }
            }
            //parse data from sentence and map
            for (j=0; j<n; j++)
            {
                data = bind_data(sentence, map, j);
                if (data >= 0)
                {
                    k++;
                    *(buffer+j*BUFSAMPLES+i) = data;
                }            }
            //reset counter
            if (k==n)
            {
                k=0;
                i++;
            }
        }
        //break if not enough samples
        if (i != BUFSAMPLES) break;
        //otherwise send it to seedlink
        for (j=0; j<n; j++)
        {
            memcpy(pack, buffer+j*BUFSAMPLES, BUFSAMPLES*sizeof(int));
            map2kfc(map, key, &field, &sr, chanid, j);
            sendrecord(id, chanid, sr, pack, timestamp);
        }
    }
    error:
        free(address);
        free(port);
        free(id);
        free(sentence);
        free(pack);
        free(timestamp);
        free(buffer);
        free(key);
        free(chanid);
    return -1;
}


int32_t bind_data(char *sentence, char *map, int index)
{
    int i, field;
    double sr;
    int32_t data;
    char *key = malloc(6);
    char *type = malloc(6);
    char *chanid = malloc(6);
    char *temp0 = strdup(sentence);
    char *temp1;

    // get keyword, field number and channel id from map and index
    map2kfc(map, key, &field, &sr, chanid, index);

    //get actual sentence type
    type = strdup(strtok(temp0, "$,*"));

    //check if keyword and sentence match
    data = -1;
    if (strcmp(key, type) == 0)
    {
        //get field data
        for (i=0; i<field; i++)
        {
            temp1 = strtok(NULL, "$,*");
            if (temp1 == NULL)
            {
                perror("ERROR in mapping definition");
                free(temp0);
                free(key);
                free(chanid);
                free(type);
                exit(0);
            }
            else data = (int32_t)(atof(temp1)*10);
        }
    }
    free(temp0);
    free(key);
    free(chanid);
    free(type);
    return data;

}


void map2kfc(char *map, char *key, int *field, double *sr, char* chanid, int index)
{
    int i;
    double j;
    char *temp0 = strdup(map);
    char *word0;
    // get keyword, field number and channel id from map and index
    word0 = strtok(temp0, ";");
    for (i=1; i<index+1; i++) word0 = strtok(NULL, ";");
    strcpy(key, strtok(word0, ",:"));
    i = atoi(strdup(strtok(NULL, ",:")));
    memcpy(field, &i, sizeof(int));
    j = atof(strdup(strtok(NULL, ",:")));
    memcpy(sr, &j, sizeof(double));
    strcpy(chanid, strtok(NULL, ",:"));
    free(temp0);}


/*Options management function:
argc: integer containing the number of arguments passed to the program (from main)
argv: tab of strings of size argc containing all the arguments passed to the program (from main)
address: pointer to a string containing the ip address, previously allocated
port: pointer to a string containing the tcp port, previously allocated
id: pointer to a string containing the station id parameter, previously allocated
map: pointer to a string containing the plugin map parameter, previously allocated

Returns 0 in case of success, -1 otherwise
*/
int options(int argc, char *argv[], char *address, char *port, char *id, char *map)
{
    int i, n;
    char *keyfile=NULL;
    char *sourceid=NULL;
    FILE *inputFile=NULL;
    char *line = malloc(MAX_STRING_LENGTH*sizeof(char));

    for (i=1; i<argc; i++)
    {
        //Version
        if (strcmp(argv[i], "-V") == 0)
        {
            printf("nmeasim_plugin version: %s\n", VERSION);
            exit(0);
        }
        //Help message
        else if (strcmp(argv[i], "-h") == 0)
        {
            printf("nmeasim_plugin version %s\n", VERSION);
            printf("Acquisition of NMEA sentences for seiscomp\n");
            printf("Usage: nmeasim_plugin [options] -s source_id -f key_file\n");
            printf("where options are:\n");
            printf("-V rep9093ort the program version\n");
            printf("-h shows this message\n");
            printf("-f specify the key file, required\n");
            printf("-s specify the source id, required\n");
            printf("Key file must contain source id between brackets, ex: [nmeasim0]\n");
            printf("The following fields are parsed until next bracket (ie other plugin config).\n");
            printf("Mandatory fields:\n");
            printf("address=\n");
            printf("port=\n");
            printf("id=\n");
            printf("map=\n");
            printf("If one of this fields is missing he program sends an error.\n");
            printf("\n");
            exit(0);
        }
        //Parse ini file path
        else if (strcmp(argv[i], "-f") == 0 && i+1 < argc)
        {
            keyfile = strdup(argv[i+1]);
        }
        //Parse plugin id
        else if (strcmp(argv[i], "-s") == 0 && i+1 < argc)
        {
            sourceid = strdup(argv[i+1]);
        }
    }
    if ((keyfile == NULL) || (sourceid == NULL))
    {
        free(line);
        free(keyfile);
        free(sourceid);
        return -1;
    }

    //Open ini file
    inputFile = fopen(keyfile, "r");
    if (inputFile == NULL)
    {
        free(line);
        free(keyfile);
        free(sourceid);
        return -1;
    }
    //Reads ini file
    i = 0;
    while (fgets(line, MAX_STRING_LENGTH, inputFile) && (i < 5))
    {
        //Pop carriage return at end of line
        n = strlen(line);
        if ((n > 0) && (line[n-1] == '\n')) line[n-1] = '\0';
        //Start parsing at source id label (between brackets)
        if (strncmp(sourceid, line+1, strlen(sourceid)) == 0)
        {
            i++;
            continue;        }
        if (i >= 1)
        {
            if (strncmp("address=", line, 8) == 0)
            {
                strcpy(address, line+8);
                i++;
            }
            if (strncmp("port=", line, 5) == 0)
            {
                strcpy(port, line+5);
                i++;
            }
            if (strncmp("id=", line, 3) == 0)
            {
                strcpy(id, line+3);
                i++;
            }
            if (strncmp("map=", line, 4) == 0)
            {
                strcpy(map, line+4);
                i++;
            }
            //Stop parsing if new plugin label is detected
            if (strncmp("[", line, 1) == 0) break;
        }

    }
    free(line);
    free(keyfile);
    free(sourceid);
    fclose(inputFile);
    //Check if number of parameters parsed is 5
    if (i != 5) return -1;
    else return 0;
}


/*Record handler function, needed by msr3_pack, calls send_mseed:
record: binary chain containing the miniseed record
length: length of record, should be 512
handlerdata: private data sent by ms3_pack to record_handler. Used here to send id to send_mseed.

Returns number of bytes sent.
*/
int record_handler(char *record, int length, void* handlerdata)
{
    return send_mseed(handlerdata, record, length);
}


/*Send record buffer
staid: station id, in format NN.SSS with NN network code and SS station code.
chanid: channel id, in format LL.CCC with LL location code and CCC channel code
buffer: samples to send.
timestamp: timestamp of first sample

Returns 0 in case of success, -1 otherwise
*/
int sendrecord(char *staid, char *chanid, double sr, int32_t *buffer, struct timespec *timestamp)
{
    MS3Record *msr = msr3_init(NULL);
    int64_t *packedsamples = malloc(BUFSAMPLES*sizeof(int32_t));
    char *net, *sta, *loc, *cha;

    net = strndup(staid, 2);
    sta = strndup(staid+3, strlen(staid)-3);
    loc = strndup(chanid, 2);
    cha = strndup(chanid+2, 3);
    msr->reclen = 512;
    ms_nslc2sid(msr->sid, LM_SIDLEN, 0, net, sta, loc, cha);
    msr->formatversion = 2;
    msr->starttime = (nstime_t)timestamp->tv_sec*1e9;
    msr->samprate = sr;
    msr->encoding = DE_INT32;
    msr->samplecnt = BUFSAMPLES;
    msr->sampletype = 'i';
    msr->datasamples = buffer;
    msr->numsamples = BUFSAMPLES;

    if (msr3_pack(msr, (void *)record_handler, (void *)staid, packedsamples, MSF_FLUSHDATA, 0) == -1)
    {
        perror("ERROR packing data");
        free(net);
        free(sta);
        free(loc);
        free(cha);
        free(msr);
        free(packedsamples);
        return -1;
    }
    free(net);
    free(sta);
    free(loc);
    free(cha);
    free(msr);
    free(packedsamples);
    return 0;
}


void sig_handler(int signum){
    printf("Leaving on signal %d\n", signum);
    go = false;
}


/*Socket connection
address: ip address.
port: tcp port.

Returns file to read data stream.
*/
FILE *sockconnect(char *address, char *port)
{
    int s;
    int sock;
    struct addrinfo hints;
    struct addrinfo *res, *rp;
    FILE *fsock;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;    /* Allow IPv4 or IPv6 */
    hints.ai_socktype = SOCK_STREAM; /* Datagram socket */
    hints.ai_flags = 0;    /* No particular flag */
    hints.ai_protocol = IPPROTO_TCP;          /* Any protocol */
    hints.ai_canonname = NULL;
    hints.ai_addr = NULL;
    hints.ai_next = NULL;

    s = getaddrinfo(address, port, &hints, &res);
    if (s != 0)
    {
        return NULL;
    }

    /* getaddrinfo() returns a list of address structures.
       Try each address until we successfully bind(2).
       If socket(2) (or bind(2)) fails, we (close the socket
       and) try the next address. */

   for (rp = res; rp != NULL; rp = rp->ai_next)
   {
        sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        // Failure
        if (sock == -1) continue;
        // Success
        if (connect(sock, rp->ai_addr, rp->ai_addrlen) != -1) break;
        close(sock);
    }

    if (rp == NULL) return NULL;
    freeaddrinfo(res);
    fsock = fdopen(sock, "r");
    return fsock;
}
