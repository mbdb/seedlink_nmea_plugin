'''
Plugin handler for the Quanterra/330 plugin.
'''
class SeedlinkPluginHandler:
  # Create defaults
  def __init__(self): pass

  def push(self, seedlink):
    # Key is per station and configuration settings
    key = ";".join([
        str(seedlink.param('sources.nmea.address')),
        str(seedlink.param('sources.nmea.port')),
        str(seedlink.param('sources.nmea.map'))])
    return key


  # Flush does nothing
  def flush(self, seedlink):
    pass
