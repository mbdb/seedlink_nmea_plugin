# seedlink_nmea_plugin

NMEA sentences acquisition plugin for seedlink/seiscomp

## Installation

nmea_plugin relies on [libmseed](https://github.com/iris-edu/libmseed).  

    git clone https://gitlab.com/mbdb/seedlink_nmea_plugin  
    cd seedlink_nmea_plugin  
    git clone https://github.com/iris-edu/libmseed  
    make

## Usage
As all seedlink plugins, nmea_plugin is able to run standalone.  

    src/nmea_plugin -s nmea0 -f plugin.ini 63>data.dat  

-s option gives a source id.  
-f option gives a config file path.  

The config file must contain a tag between brackets containing source id, followed by the following options:  
    address=  
    port=  
    id=  
    map=  

Ex:  

    [nmea0]  
    address=192.168.1.2  
    port=3000  
    id=XX.GPIL  
    map=WIMWD,1:1,00LWD

## About map option
The key to parse NMEA sentences in seedlink is the map option.  
It is a string of characters allowing the associations of NMEA fields to a specific seedlink channel.  
It format is the following:  

    SSSSS,i:j,LLCCC  

where:  
    SSSSS is the NMEA sentence type  
    i is the NMEA field number  
    j is the data sampling rate in Hz  
    LL is the seedlink location code  
    CCC is the seedlink channel code  

In the previous example (WIMWD,1:1,00LWD) means the plugin is configured to sample first field of WIMWD  
sentences (true wind direction in degrees) as 1 Hz data, chan code LWD and loc code 00.  
Several maps can be specified separated by `;`  

Ex:  
    WIMWD,1:1,00LWD;WIMWD,7:1,00LWS  

It is the operator responsibility to know what sampling rate the instrument outputs.  

## Seiscomp integration  
All needed files for seiscomp integration are provided, just run:  

    make install

The configuration of plugin can be set up via scconfig as usual,
just select nmea as source in your seedlink profile.  



